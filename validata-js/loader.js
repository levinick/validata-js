
var instance = document.querySelector('#validatajs-reference');

var plugin = document.createElement("script");
plugin.src="validata-js/validata.js";

var pluginClass = document.createElement("script");
pluginClass.src="validata-js/validata-class.js";

var iconpack = document.createElement("link");
iconpack.href= "source/font/icon.css";
iconpack.rel= "stylesheet";
iconpack.async = true;



var jquery = document.createElement('script');
jquery.src = "https://code.jquery.com/jquery-3.3.1.min.js";
jquery.async = true;

if(window.jQuery == undefined){
    document.head.appendChild(jquery);
    window.addEventListener('load', function() {
        setTimeout(function(){
            document.body.appendChild(pluginClass);
            document.body.appendChild(plugin);
            document.head.appendChild(iconpack);
        },200);
    }, true);
}else{
    document.body.appendChild(pluginClass);
    document.body.appendChild(plugin);
    document.head.appendChild(iconpack);
}

document.querySelector('#loader').remove();