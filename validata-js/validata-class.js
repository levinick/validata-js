function Validata(){

    this.genKey = function(element){

        // start attr
        var attributes = [
            // plugin attr
            ['validata-status',element.attr('validata-status'),'1'],
            ['validata-type',element.attr('validata-type'),'1'],
            ['validata-tooltip-status',element.attr('validata-tooltip-status'),'0'],
            ['validata-tooltip-text',element.attr('validata-tooltip-text'),'0'],
            ['validata-icon',element.attr('validata-icon'),'2'],
            // native attr
            ['accept',element.attr('accept'),'1'],
            ['align',element.attr('align'),'1'],
            ['alert',element.attr('alert'),'1'],
            ['autofocus',element.attr('autofocus'),'1'],
            ['autocomplete',element.attr('autocomplete'),'1'],
            ['checked',element.attr('checked'),'1'],
            ['dirname',element.attr('dirname'),'1'],
            ['disabled',element.attr('disabled'),'1'],
            ['form',element.attr('form'),'1'],
            ['formaction',element.attr('formaction'),'1'],
            ['formenctype',element.attr('formenctype'),'1'],
            ['formmethod',element.attr('formmethod'),'1'],
            ['formnovalidate',element.attr('formnovalidate'),'1'],
            ['formtarget',element.attr('formtarget'),'1'],
            ['height',element.attr('height'),'1'],
            ['list',element.attr('list'),'1'],
            ['max',element.attr('max'),'1'],
            ['maxlength',element.attr('maxlength'),'1'],
            ['min',element.attr('min'),'1'],
            ['multiple',element.attr('multiple'),'1'],
            ['name',element.attr('name'),'1'],
            ['pattern',element.attr('pattern'),'1'],
            ['placeholder',element.attr('placeholder'),'1'],
            ['readonly',element.attr('readonly'),'1'],
            ['required',element.attr('required'),'1'],
            ['size',element.attr('size'),'1'],
            ['src',element.attr('src'),'1'],
            ['step',element.attr('step'),'1'],
            ['type',element.attr('type'),'1'],
            ['value',element.attr('value'),'1'],
            ['width',element.attr('width'),'1'],
            ['id',element.attr('id'),'1'],
            ['name',element.attr('name'),'1']
        ];

        // valida values
        var validata_key_input = '';
        var validata_key_icon = '';
        var validata_key_tooltip = '';
        var retorno;

        for(i = 0;i < attributes.length; i++){
            if(attributes[i][1] != undefined){
                switch(attributes[i][2]){
                    case '1':
                        validata_key_input += attributes[i][0]+'="'+attributes[i][1]+'" ';
                        break;
                    case '0':
                        validata_key_tooltip += attributes[i][1]+'|';
                        break;
                    case '2':
                        validata_key_icon += attributes[i][1];
                        break;
                }

                // remove attr of element
                element.removeAttr(attributes[i][0]);
            }
        }

        retorno = [
            validata_key_input,
            validata_key_icon,
            validata_key_tooltip
        ]

        // return key generated
        console.log(retorno);
        return retorno;
    }

    this.genFields = function(element){
        // call genKey
        var key = this.genKey(element);

        // call builds
        var input = this.buildInput(key[0]);
        var icon = this.buildIcon(key[1]);
        var tooltip = this.buildTooltip(key[2]);

        if(element.attr('validata-tooltip-status') == 'false'){
            var content = "<div class=\"validata-fieldbox\">";
            content += "<div class=\"validata-fieldbox-icon\">"+icon+"</div>";
            content += input;
            content += "<div class=\"validata-feedback-validation\"></div>";
            content += "</div>";
        }else{
            var content = "<div class=\"validata-fieldbox\">";
            content += "<div class=\"validata-fieldbox-icon\">"+icon+"</div>";
            content += input;
            content += "<div class=\"validata-feedback-validation\">";
            content += tooltip;
            content += "</div>";
            content += "</div>";
        }

        element.append(content);
    }
    this.keyDeals = function(key){
        var hash = key.split('|');
        var vs = hash[0].split('/')[1];
        var vt = hash[1].split('/')[1];
        var vtools = hash[2].split('/')[1];
        var vtoolt = hash[3].split('/')[1];
        var keys = [vs,vt,vtools,vtoolt];
        return keys;
    }

    this.buildInput = function(hash){
        var input = '<input class="validata-fieldbox-input" '+hash+'/>';
        return input;
    }
    this.buildIcon = function(hash){
        var icon = "<i class='validata-icon'>"+hash+"</i>";
        return icon;
    }
    this.buildTooltip = function(hash){
        var item = hash.split('|');
        var tooltip = "<span class='validata-tooltip'>"+item[1]+"</span>";
        return tooltip;
    }
}