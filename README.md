# Validata-js

> Validate your data in real time...

**Validata-js** is a validation plugin in javascript with **real-time response**, no need to submit data. All its function of validation of identification of groups of characters is based on *regular expressions*.

## Installation

First, make the clone of this repository in the desired project directory.

   ```
   git clone https://levinick@bitbucket.org/levinick/validata-js.git
   ```

Include the file **validata.js** at the **bottom** of the page that the plugin will be used by pasting the code below. **(Inside the `<body>` tag)**

   ```
   <script src="validata-js/loader.js" id="loader"></script>
   <i id="validatajs-reference"></i>
   ```


## Validating 
For the plugin to identify the fields that will be affected, a series of attributes should be added as needed.

* To have only a return **without** user interaction.

  ```
  <div class="validata-field" validata-status="true" validata-type="[validation-type]" id="[...]"></div>
  ```
  
* To have a return **with** user interaction.

  ```
  <div class="validata-field" validata-status="true" validata-tooltip-status="true" validata-tooltip-text="[your-text]" validata-type="[validation-type]" id="[...]"></div>
  ```

### Attributes


 |Type| Attributes    | Default | Requeired  | Definition|
| :- | :-  | :- |:- | :- |
|_none_|*validata-field*|**null**|**true**|Sets the default css style of the field.|
|_bool_|*validata-status*|**true**|**false**|Defines whether the plugin should validate the field.|
|_string_|*validata-type*|*text*|**false**|Sets the validation parameter.|
| _bool_ | *validata-tooltip-status*|**false**|**false**| Defines whether the plug-in should show a `tooltip` when validating the field.
|_string_|_validata-tooltip-text_|_Fill in correctly_|**false**|Sets the feedback text that will appear in the tooltip.|




## License

The MIT License ([MIT](https://choosealicense.com/licenses/mit/))

Copyright (c) 2019 Vinicius Silva
